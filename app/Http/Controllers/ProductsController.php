<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();

        return view('products.list', ['products' => $products]);
    }
    
    public function featured()
    {
        // Populate this method to return a list of only featured products.
        // Check your result in the browser by visiting the /products/featured route.

        $products = Product::where('is_featured', 1)->get();

        return view('products.list', ['products' => $products]);
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        
        return view('products.show', ['product' => $product]);
    }
}
