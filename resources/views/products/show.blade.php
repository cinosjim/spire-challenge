@extends('layout')

@section('content')
    <h3>{{ $product->name }}</h3>
    <h5 class="text-muted">{{ $product->sku }}</h5>
    
    <!--
        Add a Bootstrap badge here to indicate if the
        product is a Best Seller.
    -->
    @if ($product->is_best_seller == 1)
  		<span class="badge badge-pill badge-success">BEST SELLER!</span>
  	@endif

    <p>{{ $product->description }}</p>
@endsection
